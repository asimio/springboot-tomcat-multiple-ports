package com.asimio.api.demo.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.asimio.api.demo.config.EmbeddedTomcatConfiguration;
import com.asimio.api.demo.config.FiltersConfiguration;
import com.asimio.api.demo.config.JavaMelodyConfiguration;

@SpringBootApplication(scanBasePackages = { "com.asimio.api.demo.rest" })
@Import({ EmbeddedTomcatConfiguration.class, JavaMelodyConfiguration.class, FiltersConfiguration.class })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
